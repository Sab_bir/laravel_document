require('./bootstrap');
import TurbolinksAdapter from 'vue-turbolinks';

var Turbolinks = require("turbolinks")
TurbolinksAdapter.start()


Vue.use(TurbolinksAdapter);

document.addEventListener('turbo:load', () => {
  const vueapp = new Vue({
    el: "#hello",
    template: '<App/>',
    components: { App }
  });
});

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
</head>

<body>
    <a href="{{ route('page1.index') }}">Page1</a>
    <a href="{{ route('page2.index') }}">Page2</a>
    <a href="{{ route('page3.index') }}">Page3</a>


    <div id="show_content">
        @yield('content1')
    </div>


    <script>
        $(document).ready(function() {
            var pageurl = '{{ url()->full() }}';
            console.log(pageurl);
            alert(pageurl)

            if (pageurl != "") {
                $.ajax({
                    async: false,
                    type: 'get',
                    url: pageurl,
                    beforeSend: function() {

                    },
                    success: function(data) {
                        $('#show_content').html(data);
                    }
                });
            }

        });

    </script>

</body>

</html>

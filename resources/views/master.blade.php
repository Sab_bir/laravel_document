<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/datatables.min.css') }}" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <script src="{{ asset('assets/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('assets/js/customJs/custom.js') }}"></script>
    {{-- <link rel="stylesheet" href="{{ asset('assets/css/toastr.min.css') }}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}

    <style>
        #loading {
            display: block;
            position: fixed;
            width: 100%;
            height: 100vh;
            background: hsla(180, 81%, 6%, 0.877) url("{{ asset('assets/loader.gif') }}") no-repeat center center;
            z-index: 99999;
            background-size: 100px 100px;
        }

    </style>
</head>

<body>
    {{-- onload="endLoader()"
    <div id="loading"></div> --}}
    <div id="load_page">
        @include('common_file.common_modal')
    </div>


    <a href="{{ route('index') }}">Home</a> ||
    <a href="{{ route('doctor.index') }}">Doctor</a> ||
    <a href="{{ route('student.index') }}">Student</a>||
    <a href="{{ route('student.create') }}">Student Create</a>

    @yield('content')

    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <script type="text/javascript" src="{{ asset('assets/js/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/customJs/ajaxfunction.js') }}"></script>
    {{-- <script src="{{ asset('assets/js/toastr.min.js') }}"></script>
    {!! Toastr::message() !!} --}}
    {{-- <script>
        var preloader = document.getElementById("loading");

        function endLoader() {
            preloader.style.display = 'none';
        };

        function startLoader() {
            preloader.style.display = 'block';
        };
    </script> --}}

    @stack('js')
</body>

</html>

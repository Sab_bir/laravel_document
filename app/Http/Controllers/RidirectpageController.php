<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RidirectpageController extends Controller
{
    public function page(){
        return view('redirect_page.page');
    }
    public function page1(){
        return view('redirect_page.page1');
    }
    public function page2(){
        return view('redirect_page.page2');
    }
    public function page3(){
        return view('redirect_page.page3');
    }
}

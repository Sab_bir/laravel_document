<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Axios extends Model
{
    use HasFactory;
    protected $table='axios';
    protected $fillable =[
        'name',
        'email',
    ];
}

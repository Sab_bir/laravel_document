<?php

use App\Http\Controllers\AxiosController;
use App\Http\Controllers\DatatableController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\RidirectpageController;
use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Route;




//Redirect Page
Route::get('/',[RidirectpageController::class,'page'])->name('page.index');
Route::get('/page1',[RidirectpageController::class,'page1'])->name('page1.index');
Route::get('/page2',[RidirectpageController::class,'page2'])->name('page2.index');
Route::get('/page3',[RidirectpageController::class,'page3'])->name('page3.index');




// const toaster=()=>{
//    return toastr.options = {
//         "closeButton": true,
//         "debug": false,
//         "newestOnTop": false,
//         "progressBar": true,
//         "positionClass": "toast-top-right",
//         "preventDuplicates": false,
//         "onclick": null,
//         "showDuration": "300",
//         "hideDuration": "1000",
//         "timeOut": "2000",
//         "extendedTimeOut": "1000",
//         "showEasing": "swing",
//         "hideEasing": "linear",
//         "showMethod": "fadeIn",
//         "hideMethod": "fadeOut"
//       }
// }
const  ajaxPost=(formId, modalId, dataTableId, tableId)=>{
    $(formId).submit(function (e) {
        e.preventDefault();
        const fb = new FormData(this);
        // $("#add_employee_btn").text('Addding...');
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: fb,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                startLoader()
            },
            success: function (res) {
                if (res.status == 400) {
                    $(document).find('span.error_text').text('');
                    $.each(res.errors, function (prefix, val) {
                        $('span.' + prefix + '-error').text(val[0]);
                    });
                    Command: toastr["error"]("", "Validation Error")
                    toaster();
                } else if (res.status == 200) {
                    $(document).find('span.error_text').text('');
                    $(dataTableId).DataTable().ajax.reload(null, false);
                    $(tableId).load(location.href + ' ' + tableId);
                    $(formId)[0].reset();
                    $(modalId).modal('hide');

                    Command: toastr["success"](res.message, "Success")
                    toaster()
                }
            },
            complete: function () {
                endLoader()
            }
        });
    })
}

const ajaxUpdate=(formId, modalId, dataTableId, tableId)=> {
    $(formId).submit(function (e) {
        e.preventDefault();
        const fb = new FormData(this);
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: fb,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                // startLoader()
            },
            success: function (res) {
                if (res.status == 400) {
                    $(document).find('span.edit_error_text').text('');
                    $.each(res.errors, function (prefix, val) {
                        $('span.' + prefix + '-editerror').text(val[0]);
                    });

                    Command: toastr["error"]("", "Validation Error")
                    toaster()
                } else if (res.status == 200) {
                    $(dataTableId).DataTable().ajax.reload(null, false);
                    $(tableId).load(location.href + ' ' + tableId);
                    $(formId)[0].reset();
                    $(modalId).modal('hide');
                    Command: toastr["success"](res.message, "Success")
                    toaster()
                }
            },
            complete: function () {
                // endLoader()
            }
        })
    })
}


const ajaxDelete=(btnId, dUrl, dataTableId, tableId)=> {
    $(document).on('click', btnId, function (e) {
        e.preventDefault();
        // let id = $(this).attr('id')
        let id = $(this).data('id')
        if (confirm("Are You Sure? Want to delete!!")) {
            $.ajax({
                type: "post",
                url: dUrl,
                data: {
                    id: id,
                    _token: $('input[name="_token"]').val()
                },
                beforeSend: function () {
                    startLoader()
                },
                success: function (res) {
                    $(dataTableId).DataTable().ajax.reload(null, false);
                    $(tableId).load(location.href + ' ' + tableId);
                    Command: toastr["success"](res.message, "Success")
                    toaster()
                },
                complete: function () {
                    endLoader()
                }
            });
        }

    })
}


const ajaxEdit=(editBtn,editModal,editUrl)=> {
    $(document).on('click', editBtn, function(e) {
        e.preventDefault();
        let id = $(this).data('id')
        $(editModal).modal("show");
        $.ajax({
            url: editUrl,
            method: 'get',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            beforeSend: function() {
                $(document).find('span.edit_error_text').text('');
                startLoader()
            },
            success: function(res) {
                ajaxEditData(res);
            },
            complete: function() {
                endLoader()
            }
        });
    });
}



//Store or Update
function ajaxStoreOrUpdate(formId, modalId, tableid) {
    $(formId).submit(function (e) {
        e.preventDefault();
        const fb = new FormData(this);
        $.ajax({
            async: true,
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: fb,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            },
            success: function (res) {

                if (res.status == 400) {
                    $(document).find('span.error_text').text('');
                    $.each(res.errors, function (prefix, val) {
                        $('span.' + prefix + '-error').text(val[0]);
                    });
                } else if (res.status == 200) {
                    $(document).find('span.error_text').text('');
                    $(tableid).DataTable().ajax.reload(null, false);
                    $(formId)[0].reset();
                    $(modalId).modal("hide");
                    alert(res.message)
                }
            },
            complete: function () {

            }
        });
    })
}

// Edit
const ajaxEdit2 = (editBtn, editModal, editUrl, modelheading, updatebtnVal) => {
    $(document).on('click', editBtn, function (e) {
        e.preventDefault();
        $(modelheading).html($(this).attr("data-title"))
        $(updatebtnVal).html($(this).attr('value'))
        let id = $(this).data('id')
        $(editModal).modal("show");
        $.ajax({
            url: editUrl,
            method: 'get',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            beforeSend: function () {
                $(document).find('span.error_text').text('');
                startLoader()
            },
            success: function (res) {
                ajaxEditData(res);
            },
            complete: function () {
                endLoader()
            }
        });
    });
}

// Delete
const ajaxDelete2 = (btnId, dUrl, dataTableId) => {
    $(document).on('click', btnId, function(e) {
        e.preventDefault();
        // let id = $(this).attr('id')
        let id = $(this).data('id')
        if (confirm("Are You Sure? Want to delete!!")) {
            $.ajax({
                type: "post",
                url: dUrl,
                data: {
                    id: id,
                    _token: $('input[name="_token"]').val()
                },
                beforeSend: function() {
                    startLoader()
                },
                success: function(res) {
                    $(dataTableId).DataTable().ajax.reload(null, false);
                    Command: toastr["success"](res.message, "Success")
                    toaster2()
                },
                complete: function() {
                    endLoader()
                }
            });
        }

    })
}
